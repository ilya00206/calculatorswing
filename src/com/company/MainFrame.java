package com.company;

import javax.swing.*;


public class MainFrame extends JFrame {
    private MainPanel mainPanel;
    public MainFrame() {
        mainPanel = new MainPanel();
        setBounds(100, 100, 265, 400);
        setTitle("Calculator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        add(mainPanel);
    }
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
    }
}