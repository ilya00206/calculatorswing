package com.company;

public class User {
    private String email;
    private String password;
    private History history;
    private boolean isActive;
    public User() {
        history = new History();
    }
    public boolean login() {
        return false;
    }
    public boolean logout() {
        return false;
    }
    public boolean register() {return false;}
    public boolean confirmEmail() {
        return false;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
