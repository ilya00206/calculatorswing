package com.company;

public interface CalculationInterface {
    double calculate(double n1, String op, double n2);
}
