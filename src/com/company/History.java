package com.company;

import java.util.Date;

public class History {
    private Date date;
    private String message;

    public void saveToDb(){

    }

    public History() {

    }

    public History readFromDb() {
        return null;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
