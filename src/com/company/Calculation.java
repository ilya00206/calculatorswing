package com.company;

import javax.swing.*;

public class Calculation implements CalculationInterface {
    private Logger logger;
    private User user;
    public Calculation(){
        logger = new Logger();
    }
    public Calculation(User user) {
        this.user = user;
    }
    public double calculate ( double n1, String op, double n2)
    {
        double res = 0;
        switch (op)
        {
            case "+":
                res = n1+n2;
                break;
            case "-":
                res = n1-n2;
                break;
            case "*":
                res = n1*n2;
                break;
            case "/":
                try{
                res = n1/n2; }
                catch (ArithmeticException e) {
                    JOptionPane.showMessageDialog(new JFrame(), "Cannot divide by 0", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    logger.setMessage("CANNOT DIVIDE BY 0");
                }
                break;
            default:
                res = 0;
                break;
        }
        if(user!=null) {
            user.getHistory().saveToDb();
        }
        return res;
    }
}
