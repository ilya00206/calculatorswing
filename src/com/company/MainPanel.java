package com.company;

import javax.swing.*;
import java.awt.*;

public class MainPanel  extends JPanel {
    private String operation = "";
    private double res = 0;
    private double result1 = 0;
    private Calculation calculation;

    public MainPanel()
    {
        setLayout(null);
        // Specifies the position of the element
        final TextField txt1 = new TextField();
        txt1.setBounds(10, 10, 235, 25);

        JButton b1 = new JButton("1");
        b1.setBounds(10, 190, 50, 50);

        JButton b2 = new JButton("2");
        b2.setBounds(60, 190, 50, 50);

        JButton b3 = new JButton("3");
        b3.setBounds(110, 190, 50, 50);

        JButton b4 = new JButton("4");
        b4.setBounds(10, 110, 50, 50);

        JButton b5 = new JButton("5");
        b5.setBounds(60, 110, 50, 50);

        JButton b6 = new JButton("6");
        b6.setBounds(110, 110, 50, 50);

        JButton b7 = new JButton("7");
        b7.setBounds(10, 40, 50, 50);

        JButton b8 = new JButton("8");
        b8.setBounds(60, 40, 50, 50);

        JButton b9 = new JButton("9");
        b9.setBounds(110, 40, 50, 50);

        JButton b0 = new JButton("0");
        b0.setBounds(10, 270, 50, 50);

        JButton buttonDot = new JButton(".");
        buttonDot.setBounds(60,270,50,50);

        JButton bRes = new JButton("=");
        bRes.setBounds(110, 270, 50, 50);
        Font bigFont = new Font("serif", Font.BOLD, 22);
        bRes.setFont(bigFont);

        JButton bPlus = new JButton("+");
        bPlus.setBounds(170, 40, 75, 50);
        Font bigFontPlus = new Font("serif", Font.BOLD, 22);
        bPlus.setFont(bigFontPlus);

        JButton bMinus = new JButton("-");
        bMinus.setBounds(170, 110, 75, 50);
        Font bigFontMinus = new Font("serif", Font.BOLD, 22);
        bMinus.setFont(bigFontMinus);

        JButton bMulti = new JButton("*");
        bMulti.setBounds(170, 190, 75, 50);
        Font bigFontMulti = new Font("serif", Font.BOLD, 22);
        bMulti.setFont(bigFontMulti);

        JButton bDivision = new JButton("/");
        bDivision.setBounds(170, 270, 75, 50);
        Font bigFontDivision = new Font("serif", Font.BOLD, 22);
        bDivision.setFont(bigFontDivision);

        add(txt1);
        add(b0);add(b1);add(b2);add(b3);add(b4);add(b5);
        add(b6);add(b7);add(b8);add(b9);
        add(bRes);
        add(bPlus);
        add(bMinus);
        add(bMulti);
        add(bDivision);
        add(buttonDot);

        b1.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 1);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b2.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 2);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b3.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 3);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b4.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 4);
            if (res==0)
            {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b5.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 5);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b6.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 6);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b7.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 7);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b8.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 8);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b9.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 9);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });

        b0.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 0);
            if (res==0) {
                res = Double.parseDouble(txt1.getText());
            } else {
                result1 = Double.parseDouble(txt1.getText());
            }
        });
        buttonDot.addActionListener(arg1 -> txt1.setText(txt1.getText() + "."));
        bPlus.addActionListener(arg1 -> {
            res = Double.parseDouble(txt1.getText());
            txt1.setText("");
            operation = "+";
        });

        bMinus.addActionListener(arg1 -> {
            res = Double.parseDouble(txt1.getText());
            txt1.setText("");
            operation = "-";
        });

        bMulti.addActionListener(arg1 -> {
            res = Double.parseDouble(txt1.getText());
            txt1.setText("");
            operation = "*";
        });

        bDivision.addActionListener(arg1 -> {
            res = Double.parseDouble(txt1.getText());
            txt1.setText("");
            operation = "/";
        });

        bRes.addActionListener(arg0 -> {
            double num = res;
            double num1 = result1;
            String strOp = operation;

            calculation = new Calculation();
            String strRes = String.valueOf(calculation.calculate(num, strOp, num1 ));
            txt1.setText(strRes);
        });
    }
}
