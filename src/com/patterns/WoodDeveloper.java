package com.patterns;

public class WoodDeveloper extends Developer {
    public WoodDeveloper(String name) {
        super(name);
    }

    @Override
    public House Create() {
        return new WoodHouse();
    }
}
