package com.patterns;

public class MainCompany {
    public static void main(String[] args) {
        Developer developer = new WoodDeveloper("Wood Developer");
        System.out.println(developer.getName());
        House house = developer.Create();

        developer = new BrickDeveloper("Brick developer");
        System.out.println(developer.getName());
        House house2 = developer.Create();

    }
}
