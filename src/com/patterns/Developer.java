package com.patterns;

abstract public class Developer {
    private String name;
    public Developer(String name) {
        this.name = name;
    }
    abstract public House Create();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
