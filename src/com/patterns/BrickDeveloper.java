package com.patterns;

public class BrickDeveloper extends Developer {
    public BrickDeveloper(String name) {
        super(name);
    }

    @Override
    public House Create() {
        return new BrickHouse();
    }
}
